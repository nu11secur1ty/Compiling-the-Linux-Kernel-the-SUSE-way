![](https://github.com/nu11secur1ty/Compiling-the-Linux-Kernel-the-SUSE-way/blob/master/kernel-modules.png)


# Compiling the Linux Kernel, the SUSE way by Ventsislav Varbanovski - nu11secur1ty

Share with friends and colleagues on social media
Today Linus Torvalds announced the release of the Linux Kernel 4.19 “Linux for Workgroups” (is not it funny?).
Let’s get straight to the point:
How to compile the kernel manually, SUSE style!.

For this example, I’ll use the new kernel 4.x (obviously) on a OpenSuse Leap 15 - 2018.
-----------------------------------------------------------------------------------------------

1. After all we need to install: ncurses-devel
```bash
zypper install ncurses-devel
```
2. Get the source:
```bash
wget "https://www.kernel.org/pub/linux/kernel/v4.x/linux-4.xx.tar.xz"
```
3. Unpack the source:
```bash
tar -Jxvf linux-4.xx.tar.xz -C /usr/src/
```
4. Configure the kernel:
```bash
cd /usr/src/linux-4.xx/
```
Make sure you are in the path from the source
```bash
make mrproper
```
If this is your first time, you really do not need this step, this only cleans .o files and other temporary

In the next step there are several ways to configure the kernel, I prefer to make menuconfig, but if you don’t have experience or want to play it safe, I recommend you use an existing configuration, to do this copy your old config file to the new source path:
```bash
cp /boot/config-`uname -r` .config
```
- Default
```bash
make menuconfig
```
If you still want to change or add something, at this point you can run: make menuconfig

HINT: After configuration you can further customize the kernel, adding a “extraversion” value. In the Makefile, edit the “EXTRAVERSION =” field, for (this) example:
```bash
VERSION = 4
PATCHLEVEL = xx
SUBLEVEL = 0
EXTRAVERSION = -w00t
NAME = Linux for Workgroup
```
5. Build the Kernel:
```bash
make rpm
make rpm-pkg ***new_OS_of SUSE***
```
- for new version of make:
```bash
 make rpm-pkg             - Build both source and binary RPM kernel packages
 make binrpm-pkg          - Build only the binary kernel RPM package
```
Because we copy the old configuration in this step we ask about the new kernel features, read carefully before responding.

Now, take a break, this step will take a bit depending on the performance of your hardware.

6. Install the Kernel:

If all went well, you will see output similar to this:
```bash
[...]
Wrote: /usr/src/packages/SRPMS/kernel-4.xx.x_w00t_0.11_default-1.src.rpm
Wrote: /usr/src/packages/RPMS/x86_64/kernel-4.xx.0_w00t_0.11_default-1.x86_64.rpm
Wrote: /usr/src/packages/RPMS/x86_64/kernel-headers-4.xx.0_w00t_0.11_default-1.x86_64.rpm
Executing(%clean): /bin/sh -e /var/tmp/rpm-tmp.13989
+ umask 022
+ cd /usr/src/packages/BUILD
+ cd kernel-4.xx.0_w00t_0.11_default
+ rm -rf /var/tmp/kernel-4.xx.0_w00t_0.11_default-root
+ exit 0
rm kernel-4.xx.0_w00t_0.11_default.tar.gz kernel.spec
Install the new Kernel package under /usr/src/packages/RPMS/x86_64/:
```
7. Navigate to RPMS source

```bash
cd /usr/src/packages/RPMS/x86_64/
sp3-lab:/usr/src/packages/RPMS/x86_64 # ls
kernel-4.xx.0_w00t_0.11_default-1.x86_64.rpm
kernel-headers-4.xx.0_w00t_0.11_default-1.x86_64.rpm
sp3-lab:/usr/src/packages/RPMS/x86_64 #
```
- Run command

```bash
rpm -ivh kernel-4.xx.0_w00t_0.11_default-1.x86_64.rpm
```
-or !

```bash
rpm -iUv kernel-4.xx.0_w00t_0.11_default-1.x86_64.rpm
```
And build the initrd
```bash
mkinitrd
```
Now it only remains to edit the grub menu:
```bash
vim /boot/grub/menu.lst
```
- or
```bash
grub2-mkconfig -o /boot/grub2/grub.cfg
```
And add these lines, obviously putting the right disk to your existing system:
```bash
title Open Suse Leap 15 - 4.xx Linux for Workgroups
root (hdX,X)
kernel /boot/vmlinuz-4.xx.0-w00t-0.11-default root=/dev/XXX resume=/dev/disk/XXX splash=silent crashkernel=256M-:128M showopts vga=0x314
initrd /boot/initrd-4.xx.0-w00t-0.11-default
```
