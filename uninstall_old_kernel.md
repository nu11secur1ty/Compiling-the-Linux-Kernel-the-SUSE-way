# Find installed kernels
```bash
rpm -qa | grep kernel
```
# Find the kernel image you want to remove, which should be linux-image-x.x.x-xx-generic (be sure it is).

- Then
```bash
zypper remove linux-image-x.x.x-xx-generic
```
